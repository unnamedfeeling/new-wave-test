require(['easytimer.js/dist/easytimer'], function (easytimer) {
  var Timer = easytimer.Timer;

  var timerEls=document.querySelectorAll('.js-timer'),
    timersArr=[]
  if (timerEls.length) {
    timerEls.forEach(function(t, ind, arr){

      let timeNow = Math.ceil(Date.now()/1000)

      if (t.dataset.finish==undefined) {
        t.dataset.finish=timeNow+172800
      }

      let timerDateObj=function(endTime){
    		if (endTime<timeNow) {
    			return {days: 0, hours: 0, minutes: 0, seconds: 0}
    		}

    		var diff=endTime-timeNow,
    			d=Math.floor(diff/86400),
    			h=Math.floor((diff-d*86400)/3600),
    			m=Math.floor((diff-d*86400-h*3600)/60),
    			s=Math.floor(diff-d*86400-h*3600-m*60),
    			tObj={
    				days: d,
    				hours: h,
    				minutes: m,
    				seconds: s
    			}

    		return tObj
    	}

      let timer=new Timer()
      timersArr.push(timer)

      timer.start({
				precision: 'seconds',
				countdown: true,
				// startValues: (params.endDate!==undefined)?params.endDate:{days: 1}
				startValues: timerDateObj(t.dataset.finish)
			});
      // timer.start()
      timer.addEventListener('secondsUpdated', function (e) {
        let timeValues=timer.getTimeValues()

        for (var variable in timeValues) {
          if (timeValues.hasOwnProperty(variable)) {
            if (t.querySelector('.'+variable)!==null) {
              let val=(timeValues[variable]<10)?'0'+String(timeValues[variable]):timeValues[variable]
              t.querySelector('.'+variable).innerText=val
            }
          }
        }
      });
    })
  }
});


