import Glide from '@glidejs/glide'

let conf={
    type: 'carousel',
    perView: 1,
    focusAt: 'center',
    // throttle: 60,
  },
  headerSlider=document.querySelector('.js-headerSlider'),
  unnamed_gotoSlide=function(instance, target){
    return new Promise(function(resolve, reject){
      instance.go(target)
      resolve('moved')
    })
  }

if (headerSlider&&headerSlider!==null) {
  let sl=new Glide(headerSlider, conf)

  headerSlider.querySelectorAll('.js-sliderArrow').forEach(function(arrow){
    arrow.addEventListener('click', function(event){
      unnamed_gotoSlide(sl, arrow.dataset.glideDir).then(
        function(result){
          console.log(result);
        }
      )
    })
  })

  window.addEventListener('load', function(){
    sl.mount()
  })
}

