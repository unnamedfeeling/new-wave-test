import SimpleBar from 'simplebar';
import 'simplebar/dist/simplebar.css';


let scrollBarElems=document.querySelectorAll('.js-scrollBar')

if (scrollBarElems&&scrollBarElems.length) {
  scrollBarElems.forEach((elem, ind, arr)=>{
    let sb = new SimpleBar(elem, {
      autoHide: false,
      
    });
  })
}