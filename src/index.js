
require('./styles/index.scss');
require('./assets/fonts/icomoon/style.css');

let WebFont = require('webfontloader');
WebFont.load({
  google: {
    families: ['Roboto', 'Cuprum']
  }
});

document.addEventListener("DOMContentLoaded", () => {
  require('./assets/js/timers')
  require('./assets/js/scrollBars')
  require('./assets/js/sliders')
});

window.addEventListener('load', function(){
  console.log('should load analytics here');
})
